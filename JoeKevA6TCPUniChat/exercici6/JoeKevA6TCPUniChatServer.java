package exercici6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class JoeKevA6TCPUniChatServer {
	private ArrayList<User> userList;

	private int port = 4321;

	public JoeKevA6TCPUniChatServer() {
		userList = new ArrayList<User>();
	}

	public static void main(String[] args) throws InterruptedException {
		Socket clientSocket;
		ArrayList<Socket> clientSocketList = new ArrayList<Socket>();
		ArrayList<Thread> threadList = new ArrayList<Thread>();
		JoeKevA6TCPUniChatServer server = new JoeKevA6TCPUniChatServer();
		int id = 0;

		if (args.length == 1) {
			if (args[0].equals("" + server.getPort())) {
				boolean check = true;

				try {

					ServerSocket serverSocket = new ServerSocket(server.getPort());

					do {
						System.out.println("ejecucion " + id);
						clientSocket = serverSocket.accept();
						System.out.println("aceptado");
						Thread threadProgram = new Thread(
								new JoeJuaKevA6TCPUniChatServerRunnableProgram(clientSocket, id, server));
						threadProgram.start();

					} while (check);

					serverSocket.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("Introduce un argumento (puerto)");
		}

	}

	public ArrayList<User> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public synchronized void userListSend(PrintWriter out) throws IOException {

		for (int i = 0; i < userList.size(); i++) {
			out.println(userList.get(i).getName());
		}
		out.println("DONE");
	}

	public synchronized void addUser(User user) {
		userList.add(user);
	}
}

class JoeJuaKevA6TCPUniChatServerRunnableProgram implements Runnable {

	JoeKevA6TCPUniChatServer server;
	Socket clientSocket;
	int id;

	public JoeJuaKevA6TCPUniChatServerRunnableProgram(Socket clientSocket, int id, JoeKevA6TCPUniChatServer server) {
		this.server = server;
		this.clientSocket = clientSocket;
		this.id = id;
	}

	@Override
	public void run() {
		try {
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			Thread threadLogin = new Thread(
					new JoeJuaKevA6TCPUniChatServerRunnableLogin(clientSocket, id, server, out, in));
			System.out.println("iniciado1");
			threadLogin.start();
			System.out.println("iniciado2");
			id++;

			Thread thread = new Thread(new JoeJuaKevA6TCPUniChatServerUser(out, in, server, threadLogin), "Thread ");
			thread.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

class JoeJuaKevA6TCPUniChatServerRunnableLogin implements Runnable {

	PrintWriter out;
	BufferedReader in;
	JoeKevA6TCPUniChatServer server;
	Socket clientSocket;
	int id;

	public JoeJuaKevA6TCPUniChatServerRunnableLogin(Socket clientSocket, int id, JoeKevA6TCPUniChatServer server,
			PrintWriter out, BufferedReader in) {
		this.server = server;
		this.clientSocket = clientSocket;
		this.id = id;
		this.out = out;
		this.in = in;
	}

	@Override
	public void run() {
		try {
			String nombre, contraseña;

			System.out.println("leyendo");
			nombre = in.readLine();
			contraseña = in.readLine();
			System.out.println(nombre);
			System.out.println(contraseña);

			User user = new User(id, nombre, contraseña, clientSocket.getInetAddress().toString(),
					clientSocket.getLocalPort(), clientSocket);
			System.out.println("Usuario creado");
			server.addUser(user);
			System.out.println("Usuario añadido");

			server.userListSend(out);

			System.out.println("antes refresh");

			String refresh = in.readLine();

			System.out.println("Refresh recibido: " + refresh);

			if (refresh.equals("REFRESH")) {
				System.out.println("if refresh");
				server.userListSend(out);
			}
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println("excepcio test");
		}
		System.out.println("surt");
	}

}

class JoeJuaKevA6TCPUniChatServerUser implements Runnable {

	JoeKevA6TCPUniChatServer server;
	String userSend;
	PrintWriter outActualClient;
	BufferedReader inActualClient;
	Thread threadLogin;

	public JoeJuaKevA6TCPUniChatServerUser(PrintWriter out, BufferedReader in, JoeKevA6TCPUniChatServer server,
			Thread threadLogin) {
		this.threadLogin = threadLogin;
		this.server = server;
		outActualClient = out;
		inActualClient = in;
	}

	@Override
	public void run() {

		try {
			threadLogin.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		try {
			boolean repeatChat = false;

			do {
				while ((userSend = inActualClient.readLine()) != null) {
					System.out.println("Usuario recibido: " + userSend);
					repeatChat = false;
					boolean clientExist = false;

					for (int i = 0; i < server.getUserList().size(); i++) {
						System.out.println("buscando usuario");
						if (server.getUserList().get(i).getName().equals(userSend)) {
							System.out.println("usuario encontrado");
							clientExist = true;

							Socket clientSendSocket = server.getUserList().get(i).getSocket();
							PrintWriter outSendClient = new PrintWriter(clientSendSocket.getOutputStream(), true);

							String message = "";

							do {
								message = inActualClient.readLine();
								if (message.equals("REPEAT")) {
									System.out.println("REPEAT");
									repeatChat = true;
									message = null;
								} else {
									System.out.println("mensaje para " + server.getUserList().get(i).getName()
											+ " recibido: " + message);
									outSendClient.println(message);
								}
							} while (message != null);
							System.out.println("chat fin");
						}
					}

					if (!clientExist) {
						outActualClient.println("Usuari not found");
					}

				}
			} while (repeatChat);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}