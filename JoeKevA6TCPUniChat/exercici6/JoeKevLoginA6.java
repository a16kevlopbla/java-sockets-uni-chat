package exercici6;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;

public class JoeKevLoginA6 extends JPanel implements ActionListener {
	private JTextField textField;
	private JTextField textField_1;
	PrintWriter out;
	BufferedReader in;

	/**
	 * Create the panel.
	 */
	public JoeKevLoginA6(PrintWriter out, BufferedReader in) {
		
		
		this.out = out;
		this.in = in;
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		setPreferredSize(new Dimension(350, 180));

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		horizontalStrut_2.setPreferredSize(new Dimension(50, 0));
		add(horizontalStrut_2);

		JLabel lblNewLabel = new JLabel("Username");
		add(lblNewLabel);

		textField = new JTextField();
		add(textField);
		textField.setColumns(10);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		horizontalStrut_3.setPreferredSize(new Dimension(50, 0));
		add(horizontalStrut_3);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setPreferredSize(new Dimension(50, 0));
		add(horizontalStrut);

		JLabel lblNewLabel_1 = new JLabel("Password");
		add(lblNewLabel_1);

		textField_1 = new JTextField();
		add(textField_1);
		textField_1.setColumns(10);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		horizontalStrut_1.setPreferredSize(new Dimension(50, 0));
		add(horizontalStrut_1);

		JButton buttonLogin = new JButton("Login");
		add(buttonLogin);
		buttonLogin.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (textField.getText().length()>1 && textField_1.getText().length()>1) {

			String user = textField.getText();
			String password = textField_1.getText();

			JoeKevA6TCPUniChatClient.sendLoginData(user, password, out, in);

			JoeKevFrameA6 jchat = ((JoeKevFrameA6) getParent().getParent().getParent());
			JoeKevSelectUserA6 panelselectuser = new JoeKevSelectUserA6(user, out, in);
			jchat.setContentPane(panelselectuser);
			jchat.pack();
			
			panelselectuser.setThispanel(panelselectuser);
			
			

		}
	}

}
