package exercici6;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.PrintWriter;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class JoeKevFrameA6 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					JoeJuaKevChatA6 frame = new JoeJuaKevChatA6();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public JoeKevFrameA6(PrintWriter out, BufferedReader in) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JoeKevLoginA6(out, in);
		setTitle("JoelKevin_Unichat");

		setContentPane(contentPane);
		pack();
	}

}
