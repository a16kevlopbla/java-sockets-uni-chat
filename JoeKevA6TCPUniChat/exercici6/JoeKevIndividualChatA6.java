package exercici6;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.xml.ws.handler.MessageContext;
import javax.swing.JSplitPane;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.TextArea;
import javax.swing.JEditorPane;
import java.awt.Font;

public class JoeKevIndividualChatA6 extends JPanel implements ActionListener {
	private JTextField textField;
	PrintWriter out;
	BufferedReader in;
	JEditorPane editorPane;
	String selecteduser;
	String userowner;
	JoeKevSelectUserA6 panel;
	
	/**
	 * Create the panel.
	 */
	public JoeKevIndividualChatA6(JoeKevSelectUserA6 panel,String userowner, String selecteduser, PrintWriter out, BufferedReader in) {
		setPreferredSize(new Dimension(600, 400));

		this.panel=panel;
		this.selecteduser = selecteduser;
		this.out = out;
		this.in = in;
		this.userowner=userowner;

		JoeKevA6Thread t1 = new JoeKevA6Thread();
		t1.start();

		setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel("User");
		lblNewLabel.setFont(new Font("Laksaman", Font.BOLD, 18));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblNewLabel, BorderLayout.NORTH);
		lblNewLabel.setText("Chating with " + selecteduser);

		JSplitPane splitPane = new JSplitPane();
		add(splitPane, BorderLayout.SOUTH);

		textField = new JTextField();
		textField.setToolTipText("asgf");
		splitPane.setRightComponent(textField);
		textField.setColumns(10);

		JButton btnSend_1 = new JButton("SEND");
		splitPane.setLeftComponent(btnSend_1);

		btnSend_1.addActionListener(this);

		editorPane = new JEditorPane();
		editorPane.setFont(new Font("Sawasdee", Font.BOLD, 14));
		editorPane.setEditable(false);
		add(editorPane, BorderLayout.CENTER);

		JButton btnNewButton = new JButton("BACK");
		add(btnNewButton, BorderLayout.WEST);
		btnNewButton.addActionListener(this);
		btnNewButton.setActionCommand("back");
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals("back")) {

			out.println("REPEAT");


			JoeKevFrameA6 jchat = ((JoeKevFrameA6) getParent().getParent().getParent());
				
			jchat.setContentPane(panel);			
			jchat.pack();
			
			


		} else {
			String mensaje = textField.getText();

			JoeKevA6TCPUniChatClient.sendMessage(mensaje, out, in);

			editorPane.setText(editorPane.getText() + "Tu: " + mensaje + "\n");

			textField.setText("");
		}
	}

	class JoeKevA6Thread extends Thread implements Runnable {

		public JoeKevA6Thread() {

		}

		@Override
		public void run() {

			String message = null;
			boolean exit = true;

			do {

				try {
					message = in.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}

				editorPane.setText(editorPane.getText() + selecteduser + ": " + message + "\n");

			} while (exit);

			try {
				in.close();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

}
