package exercici6;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.awt.FlowLayout;
import java.awt.Component;
import javax.swing.Box;

public class JoeKevSelectUserA6 extends JPanel implements ActionListener {

	JoeKevSelectUserA6 thispanel;

	public JoeKevSelectUserA6 getThispanel() {
		return thispanel;
	}

	public void setThispanel(JoeKevSelectUserA6 thispanel) {
		this.thispanel = thispanel;
	}

	private JComboBox comboBox;
	private List<String> aUser;
	PrintWriter out;
	BufferedReader in;
	String user;
	String userowner;

	/**
	 * Create the panel.
	 */
	public JoeKevSelectUserA6(String user, PrintWriter out, BufferedReader in) {

		this.out = out;
		this.in = in;
		this.user = user;
		this.userowner = user;

		setPreferredSize(new Dimension(250, 200));

		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		aUser = new ArrayList<String>();
		aUser = JoeKevA6TCPUniChatClient.getArrayUsers(out, in);

		JLabel lblNewLabel = new JLabel(this.user + " select user:");
		add(lblNewLabel);

		comboBox = new JComboBox(aUser.toArray());
		comboBox.setPreferredSize(new Dimension(100, 24));
		add(comboBox);

		JButton buttonrefresh = new JButton("REFRESCAR USUARIOS");
		add(buttonrefresh);
		buttonrefresh.setActionCommand("refresh");
		buttonrefresh.addActionListener(this);

		JButton buttonchatear = new JButton("CHATEAR");
		buttonchatear.setPreferredSize(new Dimension(96, 40));
		add(buttonchatear);
		buttonchatear.setActionCommand("chat");
		buttonchatear.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getActionCommand() == "refresh") {

			List<String> aUsernew = new ArrayList<String>();

			aUsernew = JoeKevA6TCPUniChatClient.refreshUsers(out, in);

			comboBox.setModel(new DefaultComboBoxModel(aUsernew.toArray()));

		} else {

			String selectedUser = (String) comboBox.getItemAt(comboBox.getSelectedIndex());

			JoeKevA6TCPUniChatClient.selectUsertoChat(selectedUser, out, in);

			JoeKevFrameA6 jchat = ((JoeKevFrameA6) getParent().getParent().getParent());
			jchat.setContentPane(new JoeKevIndividualChatA6(thispanel, userowner, selectedUser, out, in));
			jchat.pack();
		}
	}

}
