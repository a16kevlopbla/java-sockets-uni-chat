package exercici6;

import java.net.Socket;

public class User {
	private int id;
	private String name;
	private String pass;
	private String IP;
	private int port;
	private Socket socket;

	public User(int id, String nombre, String pass, String IP, int port, Socket socket) {
		this.id = id;
		this.name = nombre;
		this.pass = pass;
		this.IP = IP;
		this.socket = socket;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

}
