package exercici6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class JoeKevA6TCPUniChatClient {

	static String ipServer = "192.168.204.123";
	static int portServer = 4321;

	public static void main(String[] args) throws UnknownHostException {

		try {

			// REFRESH BUTTON SÓLO DARLE 1 VEZ
			
			Socket socket;
			PrintWriter out;
			BufferedReader in;

			socket = new Socket(ipServer, portServer);

			out = new PrintWriter(socket.getOutputStream(), true);

			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			JoeKevFrameA6 frame = new JoeKevFrameA6(out, in);
			frame.setVisible(true);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void sendLoginData(String user, String password, PrintWriter out, BufferedReader in) {

		out.println(user);
		out.println(password);

	}

	public static List<String> getArrayUsers(PrintWriter out, BufferedReader in) {

		List<String> aUser = new ArrayList<String>();

		try {

			String nombre;
			do {

				nombre = in.readLine();
				if (!nombre.equals("DONE")) {
					aUser.add(nombre);
				}

			} while (!nombre.equals("DONE"));

		} catch (IOException e) {
			e.printStackTrace();
		}

		return aUser;
	}

	public static List<String> refreshUsers(PrintWriter out, BufferedReader in) {

		List<String> aUser = new ArrayList<String>();

		try {
			out.println("REFRESH");
			String newuser;
			do {

				newuser = in.readLine();
				if (!newuser.equals("DONE")) {
					aUser.add(newuser);
				}

			} while (!newuser.equals("DONE"));

		} catch (IOException e) {
			e.printStackTrace();
		}

		return aUser;

	}

	public static void selectUsertoChat(String user, PrintWriter out, BufferedReader in) {

		out.println(user);

	}

	public static void sendMessage(String message, PrintWriter out, BufferedReader in) {

		out.println(message);

	}

}



